const mongoose = require('mongoose')
const fetch = require('node-fetch')
const debug = require('debug')('script:seedDB')
const moment = require('moment')
const _ = require('lodash')
const querystring = require('querystring')
const Neo = require('../models/neo')

const config = require('../config')

mongoose.Promise = Promise

module.exports = start

if (config.env !== 'test') { // start the script automatically if not in test env
  start()
}
function start () {
  debug('Starting script.')
  return connectandClearDB()
    .then(() => {
      const startDate = moment().subtract(3, 'days').format('YYYY-MM-DD')
      const endDate = moment().format('YYYY-MM-DD')
      return fetchData(config.NASA_API_KEY, startDate, endDate)
    })
    .then(transFormData)
    .then(saveToDB)
    .then((response) => {
      if (process.env.NODE_ENV !== 'test') {
        console.log('Process completed!')
        process.exit(0)
      }
    })
}

/**
 * Fetch Data from NASA's Near Earth Object (NEO) API
 * @param {string} apiKey api Key to authenticate requests to NASA Gov
 * @param {string} startDate startDate query to send to API. Format 'YYYY-MM-DD'
 * @param {string} endDate endDate query to send to API. Format 'YYYY-MM-DD'
 * @return {Promise<NEODataObj>} Response from NASA's NEO API Sample seen here https://api.nasa.gov/neo/
 */
function fetchData (apiKey, startDate, endDate) {
  if (!apiKey && !startDate && !endDate) {
    throw new Error('Required parameters missing (apiKey, startDate, endDate)')
  }
  const baseUrl = 'https://api.nasa.gov/neo/rest/v1/feed'
  const query = querystring.stringify({
    start_date: startDate,
    end_date: endDate,
    detailed: 'true',
    api_key: apiKey,
  })
  debug(
    'Fetching data from the NASA API with start_date %s and end_date %s',
    startDate,
    endDate
  )
  return fetch(`${baseUrl}?${query}`)
    .then(res => res.json())
}

/**
 * @description Save Data to DB
 * @param {Array<NEO>} - data NEO model compatible data.
 * @return {Promise<Array<NEO>>} - Array of NEO's data saved to the DB
 */
function saveToDB (data) {
  debug('Saving Data to DB.')
  debug('Total data to be saved: %d', data.length)
  return Neo.insertMany(data)
}

/**
 * @description Connect to MongoDB and clear DB.
 * @return {Promise<Array<NEO>>} - resolves to Array of Data removed from the DB
 */
function connectandClearDB () {
  // if we are connected to mongodb already or trying to connect, no need to reestablish new connection.
  if (!mongoose.connection.db && mongoose.connection.readyState !== 2) {
    mongoose.connect(config.mongoURI, {
      useMongoClient: true,
    })
  }
  return Neo.remove({})
}

/**
 * @description Transform data from format gotten from NASA API to what is expected by NEO Model, according to the Schema
 * @param {Array<Object>} data JSON Response from the API
 * @return {Array<NEO>} Transformed data according to the NEO schema.
 */
function transFormData (data) {
  debug('Transforming data')
  return new Promise((resolve) => {
    const result = []
    _.each(data['near_earth_objects'], (value, date) => {
      _.each(value, (obj) => {
        const neo = { date }
        neo.reference = obj['neo_reference_id']
        neo.name = obj.name
        neo.speed = obj['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']
        neo.isHazardous = obj['is_potentially_hazardous_asteroid']
        result.push(neo)
      })
    })
    resolve(result)
  })
}
