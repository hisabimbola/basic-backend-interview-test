var mongoose = require('mongoose')
var Schema = mongoose.Schema

var NeoSchema = new Schema({
  date: Date,
  reference: String,
  name: String,
  speed: {
    type: Number,
    description: 'Speed of Neo in kilometers per hour',
  },
  isHazardous: Boolean,
})

const Neo = mongoose.model('Neo', NeoSchema)
module.exports = Neo
