const express = require('express')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const swaggerValidator = require('swagger-tools/middleware/swagger-validator')
const swaggerUi = require('swagger-tools/middleware/swagger-ui')

const swaggerDoc = require('./api-docs.json')
const routes = require('./routes')
const neoRoutes = require('./routes/neo')

const config = require('./config')
const app = express()

mongoose.Promise = Promise

mongoose.connect(config.mongoURI, {
  useMongoClient: true,
})

app.use(logger('dev', {
  skip: (req, res) => {
    return app.get('env') !== 'development'
  },
}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

// Validate Swagger requests
app.use(swaggerValidator())

// Serve the Swagger documents and Swagger UI
app.use(swaggerUi(swaggerDoc))

app.use('/', routes)
app.use('/neo', neoRoutes)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res) {
    res.status(err.status || 500)
    res.json({
      message: err.message,
      error: err,
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res) {
  res.status(err.status || 500)
  res.json({
    message: err.message,
    error: {},
  })
})

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
  // application specific logging, throwing an error, or other logic here
  process.exit(1) // do something more fancy here
})

module.exports = app
