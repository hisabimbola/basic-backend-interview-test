FROM node:boron

RUN mkdir /app
WORKDIR /app

ENV NPM_CONFIG_LOGLEVEL warn

COPY package.json .

RUN npm install --quiet

COPY . .

EXPOSE 3000

CMD [ "npm", "start"]
