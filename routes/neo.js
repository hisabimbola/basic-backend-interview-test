const express = require('express')
const router = express.Router()
const neoController = require('../controllers/neo')

router.get('/hazardous', neoController.allPotentialHazardousNeo)
router.get('/fastest', neoController.fastestNeo)
router.get('/best-year', neoController.bestYear)
router.get('/best-month', neoController.bestMonth)

module.exports = router
