const express = require('express')
const router = express.Router()
const baseController = require('../controllers')

/* GET home page. */
router.get('/', baseController.index)

module.exports = router
