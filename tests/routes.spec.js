const request = require('supertest')
const mongoose = require('mongoose')
const moment = require('moment')
const _ = require('lodash')
const Neo = require('../models/neo')
const app = require('../app')
const seedDB = require('../scripts/seedDB')

describe('Routes', () => {
  beforeAll(() => {
    return seedDB()
  }, 10000)

  afterAll(() => {
    // Disconnect from mongodb, so that test can exit
    mongoose.disconnect()
  })

  describe('Home', () => {
    it('should response the GET method', () => {
      return request(app).get('/').then((response) => {
        expect(response.statusCode).toBe(200)
        expect(response.body).toEqual({ hello: 'world' })
      })
    })
  })

  describe('Neo', () => {
    describe('fetch potentially hazardous asteriods', () => {
      it('should return statusCode 200', () => {
        return request(app).get('/neo/hazardous').then(({ statusCode }) => {
          expect(statusCode).toBe(200)
        })
      })
      it('should return data with property isHazardous: true', () => {
        return request(app).get('/neo/hazardous').then(({ body }) => {
          body.forEach((result) => {
            expect(result.isHazardous).toBe(true)
          })
        })
      })
    })
    describe('fetch Fastest Asteroid', () => {
      it('should return statusCode 200', () => {
        return request(app).get('/neo/fastest').then(({ statusCode }) => {
          expect(statusCode).toBe(200)
        })
      })
      it('should use hazardous parameters of false by default', () => {
        return request(app).get('/neo/fastest').then(({ body }) => {
          expect(body).toHaveProperty('isHazardous', false)
        })
      })
      it('should accept hazardous query parameter', () => {
        return request(app).get('/neo/fastest?hazardous=true').then(({ body }) => {
          expect(body).toHaveProperty('isHazardous', true)
        })
      })
      describe('Fastest Asteroid', () => {
        let fastestNeo
        const highestSpeed = Number.MAX_SAFE_INTEGER // Hopefully no Asteroid will reach this speed soon
        beforeAll(() => {
          fastestNeo = new Neo()
          fastestNeo.speed = highestSpeed
          fastestNeo.isHazardous = false
          return fastestNeo.save()
        })
        afterAll(() => {
          return fastestNeo.remove()
        })
        it('should return the fastestNeo', () => {
          return request(app).get('/neo/fastest').then(({ body }) => {
            expect(body).toEqual(expect.objectContaining({
              speed: highestSpeed,
              isHazardous: false,
            }))
          })
        })
      })
    })

    describe('fetch best year', () => {
      let threeYearsAgo
      let threeYearsNum
      let fourYearsAgo
      let fourYearsNum
      const ids = []
      beforeAll(() => {
        threeYearsAgo = moment().subtract(3, 'years')
        threeYearsNum = threeYearsAgo.year()
        fourYearsAgo = moment().subtract(4, 'years')
        fourYearsNum = fourYearsAgo.year()
        const neoMock = []
        _.range(100).forEach(() => {
          neoMock.push({
            date: threeYearsAgo.format('YYYY-MM-DD'),
            isHazardous: true,
          })
        })
        _.range(40).forEach(() => {
          neoMock.push({
            date: fourYearsAgo.format('YYYY-MM-DD'),
            isHazardous: false,
          })
        })
        return Neo.insertMany(neoMock).then((result) => {
          result.forEach((neo) => {
            ids.push(neo._id)
          })
        })
      })
      afterAll(() => {
        return Neo.deleteMany({ _id: { $in: ids } })
      })
      it('should return statusCode 200', () => {
        return request(app).get('/neo/best-year').then(({ statusCode }) => {
          expect(statusCode).toBe(200)
        })
      })
      it('should return data with the top number of asteriods', () => {
        return request(app).get('/neo/best-year').then(({ body: [bestYear] }) => {
          expect(bestYear).toEqual(expect.objectContaining({
            _id: fourYearsNum,
            count: expect.any(Number),
          }))
        })
      })
      it('should accept hazardous query parameter', () => {
        return request(app)
          .get('/neo/best-year?hazardous=true')
          .then(({ body: [bestYear] }) => {
            expect(bestYear).toEqual(expect.objectContaining({
              _id: threeYearsNum,
              count: expect.any(Number),
            }))
          })
      })
    })

    describe('fetch best month', () => {
      let threeMonthsAgo
      let threeMonthsNum
      let fourMonthsAgo
      let fourMonthsNum
      const ids = []
      beforeAll(() => {
        threeMonthsAgo = moment().subtract(3, 'months')
        threeMonthsNum = threeMonthsAgo.month() + 1
        fourMonthsAgo = moment().subtract(4, 'months')
        fourMonthsNum = fourMonthsAgo.month() + 1
        const neoMock = []
        _.range(100).forEach(() => {
          neoMock.push({
            date: threeMonthsAgo.format('YYYY-MM-DD'),
            isHazardous: true,
          })
        })
        _.range(40).forEach(() => {
          neoMock.push({
            date: fourMonthsAgo.format('YYYY-MM-DD'),
            isHazardous: false,
          })
        })
        return Neo.insertMany(neoMock).then((result) => {
          result.forEach((neo) => {
            ids.push(neo._id)
          })
        })
      })
      afterAll(() => {
        return Neo.deleteMany({ _id: { $in: ids } })
      })
      it('should return statusCode 200', () => {
        return request(app).get('/neo/best-month').then(({ statusCode }) => {
          expect(statusCode).toBe(200)
        })
      })
      it('should return data with the top number of asteriods', () => {
        return request(app).get('/neo/best-month').then(({ body: [bestYear] }) => {
          expect(bestYear).toEqual(expect.objectContaining({
            _id: fourMonthsNum,
            count: expect.any(Number),
          }))
        })
      })
      it('should accept hazardous query parameter', () => {
        return request(app)
          .get('/neo/best-month?hazardous=true')
          .then(({ body: [bestYear] }) => {
            expect(bestYear).toEqual(expect.objectContaining({
              _id: threeMonthsNum,
              count: expect.any(Number),
            }))
          })
      })
    })
  })
})
