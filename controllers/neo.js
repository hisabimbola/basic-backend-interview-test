const Neo = require('../models/neo')
const debug = require('debug')('neo:controllers:neo')
/**
 * @swagger
 * definitions:
 *   Neo:
 *     type: object
 *     properties:
 *       date:
 *         type: string
 *       reference:
 *         type: string
 *       name:
 *         type: string
 *       speed:
 *         type: integer
 *       isHazardous:
 *         type: boolean
 *   Neos:
 *     allOf:
 *       - $ref: '#/definitions/Neo'
 *
 *   BestYear:
 *     type: object
 *     properties:
 *       count:
 *         type: integer
 *       _id:
 *         type: integer
 *
 *   BestYearRes:
 *     type: array
 *     items:
 *       $ref: '#/definitions/BestYear'
 *
 *   BestMonth:
 *     type: object
 *     properties:
 *       count:
 *         type: integer
 *       _id:
 *         type: integer
 *
 *   BestMonthRes:
 *     type: array
 *     items:
 *       $ref: '#/definitions/BestMonth'
 *
 * /neo/hazardous:
 *   get:
 *     description: Display all DB entries which contain potentially hazardous asteriods
 *     tags: [Neo]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: All DB entries which contain potentially hazardous asteriods
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Neos'
 */
exports.allPotentialHazardousNeo = function (req, res) {
  const query = {
    isHazardous: true,
  }
  Neo.find(query).exec().then((results) => {
    debug('Fetched %d number of potentially hazardousNeo', results.length)
    res.json(results)
  }).catch((error) => {
    debug(
      'Unexpected error occured while fetching potentially hazardous neo: %O',
      error
    )
    res.status(500).send(error)
  })
}

/**
 * @swagger
 * /neo/fastest:
 *   get:
 *     description: Returns the fastest asteroid.
 *     tags: [Neo]
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: hazardous
 *         type: integer
 *         description: Filter base on hazardous Neo or not.
 *         default: false
 *         enum: [true, false]
 *     responses:
 *       200:
 *         description: All DB entries which contain potentially hazardous asteriods
 *         schema:
 *           $ref: '#/definitions/Neo'
 */
exports.fastestNeo = function (req, res) {
  let hazardous = (req.query.hazardous === 'true')
  debug('Fetching fastest Neo with query: {hazardous: %s}', hazardous)
  Neo.findOne({ isHazardous: hazardous })
    .sort({speed: -1})
    .exec()
    .then((result) => {
      if (result) {
        debug('Fastest Neo is %s', result.name)
      }
      res.json(result)
    }).catch((error) => {
      debug(
        'Unexpected error occured while fetching potentially hazardous neo: %O',
        error
      )
      res.status(500).send(error)
    })
}

/**
 * @swagger
 * /neo/best-year:
 *   get:
 *     description: Returns a year with the most asteriods.
 *     tags: [Neo]
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: hazardous
 *         type: integer
 *         description: Filter base on hazardous Neo or not.
 *         default: false
 *         enum: [true, false]
 *     responses:
 *       200:
 *         description: All DB entries which contain potentially hazardous asteriods
 *         schema:
 *           $ref: '#/definitions/BestYearRes'
 */
exports.bestYear = function (req, res) {
  let hazardous = (req.query.hazardous === 'true')
  debug('Fetching bestYear Neo with query: {hazardous: %s}', hazardous)
  Neo.aggregate([{
    $match: { isHazardous: hazardous },
  }, {
    $group: {
      _id: { $year: '$date' },
      count: { $sum: 1 },
    },
  }, {
    $sort: { count: -1 },
  }, {
    $limit: 1,
  }]).then((result) => {
    res.json(result)
  }).catch((error) => {
    debug(
      'Unexpected error occured while fetching potentially hazardous neo: %O',
      error
    )
    res.status(500).send(error)
  })
}

/**
 * @swagger
 * /neo/best-month:
 *   get:
 *     description: Return a month with most asteriods.
 *     tags: [Neo]
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: hazardous
 *         type: integer
 *         description: Filter base on hazardous Neo or not.
 *         default: false
 *         enum: [true, false]
 *     responses:
 *       200:
 *         description: All DB entries which contain potentially hazardous asteriods
 *         schema:
 *           $ref: '#/definitions/BestMonthRes'
 */
exports.bestMonth = function (req, res) {
  let hazardous = (req.query.hazardous === 'true')
  debug('Fetching bestYear Neo with query: {hazardous: %s}', hazardous)
  Neo.aggregate([{
    $match: { isHazardous: hazardous },
  }, {
    $group: {
      _id: { $month: '$date' },
      count: { $sum: 1 },
    },
  }, {
    $sort: { count: -1 },
  }, {
    $limit: 1,
  }]).then((result) => {
    res.json(result)
  }).catch((error) => {
    debug(
      'Unexpected error occured while fetching potentially hazardous neo: %O',
      error
    )
    res.status(500).send(error)
  })
}
