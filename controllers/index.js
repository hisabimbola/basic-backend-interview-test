/**
 * @swagger
 * /:
 *   get:
 *     description: Returns the homepage
 *     responses:
 *       200:
 *         description: hello world
 */
exports.index = function respond (req, res) {
  res.json({hello: 'world'})
}
