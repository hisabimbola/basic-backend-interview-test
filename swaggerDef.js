const pkg = require('./package.json')

module.exports = {
  info: {
    title: pkg.name,
    version: pkg.version,
    description: pkg.description,
  },
  basePath: '/',

  apis: ['./controllers/*.js'],
}
