const config = {
  development: {
    env: process.env.NODE_ENV || 'development',
    mongoURI: 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/neo-dev',
    NASA_API_KEY: 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD',
    port: process.env.PORT || 8000,
  },
  test: {
    env: process.env.NODE_ENV || 'test',
    mongoURI: 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/neo-test',
    NASA_API_KEY: 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD',
    port: process.env.PORT || 5555,
  },
}

function initConfig (env) {
  return config[env]
}

const env = process.env.NODE_ENV || 'development'
module.exports = initConfig(env)
